# Designs

Designs by me, or adapted by me, released as CC-BY-SA

Files are always released as .SVG, and can be freely modified by using software such as [Inkscape](https://inkscape.org/)